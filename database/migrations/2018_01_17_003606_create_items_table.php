<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('vendor_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('owner_id')->unsigned();
            $table->string('serial')->nullable();
            $table->string('price')->nullable();
            $table->string('weight')->nullable();
            $table->string('color')->nullable();
            $table->string('photo')->nullable();
            $table->date('release_date')->nullable();
            $table->timestamp('created_at')->nullable();
            
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
