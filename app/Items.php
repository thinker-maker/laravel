<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Items model
 *
 * @author Pavlo Gumeniuk <pavlo.g@scopicsoftware.com>
 */
class Items extends Model
{
    use \Conner\Tagging\Taggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'vendor_id',
        'type_id',
        'serial',
        'price',
        'weight',
        'color',
        'photo',
        'tags',
        'release_date'
    ];
    
    public $timestamps = false;

    /**
     * Get the user that owns the item.
     */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    /**
     * Get the vendor that owns the item.
     */
    public function vendor()
    {
    	return $this->belongsTo(Vendor::class);
    }

    /**
     * Get the type that owns the item.
     */
    public function type()
    {
    	return $this->belongsTo(Types::class);
    }
}
