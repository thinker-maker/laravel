<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function($table) {
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade');
        });

        Schema::table('vendors', function($table) {
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
        });

        DB::table('types')->insert(
            [
                'id' => 1,
                'name' => 'Phone'
            ]
        );

        DB::table('types')->insert(
            [
                'id' => 2,
                'name' => 'Tablet'
            ]
        );

        DB::table('types')->insert(
            [
                'id' => 3,
                'name' => 'Laptop'
            ]
        );

        DB::table('users')->insert(
            [
                'id' => 1,
                'username' => 'admin',
                'password' => '$2y$10$GDNoPLXPWKMI3XcySWgZqecH24H7JFlh/X46iSiuC9xr60qXiGyFy',
                'email' => 'admin@example.com',
                'admin' => 1,
                'active' => 1
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
