@extends('layouts.app')

@section('content')
<div class="col-md-8 col-md-offset-2">
        <h2 class="text-center">Manage Users</h2>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-user"></i> All the current Users</li>
                    <a href="#" class="add-modal"><li>Add User</li></a>
                </ul>
            </div>

            <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="usersTable">
                        <thead>
                            <tr>
                                <th valign="middle">ID</th>
                                <th>Usename</th>
                                <th>Email</th>
                                <th>Added</th>
                                <th>Admin</th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            @foreach($users as $user)
                                <tr id="<?= $user->id?>">
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->username}}</td>
                                    <td>{{$user->email}}</td>        
                                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->diffForHumans() }}</td>
                                    <td class="text-center"><input type="checkbox" @if ($user->admin) checked @endif></td>
                                    <td class="text-center">
                                        <button type="button" class="btn activate <?= $user->active ? 'btn-red' : 'btn-green'?>">
                                            <?= $user->active ? 'Deactivate' : 'Activate'
                                            ?>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->

<!-- Modal form to add a post -->
<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <p class="errorTitle text-left alert alert-danger hidden"></p>
                            <p class="successTitle text-left alert alert-success hidden"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <strong>Username:</strong>
                                {!! Form::text('username', null, array('id' => 'username','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <strong>Email:</strong>
                                {!! Form::text('email', null, array('id' => 'email','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <strong>Password:</strong>
                                {!! Form::password('password', array('id' => 'password', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <strong>Repeat Password:</strong>
                                {!! Form::password('password_confirmation', array('id' => 'password_confirmation','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <strong>Active:</strong>
                                {!! Form::checkbox('active', 1, true, ['id' => 'active']) !!}
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <strong>Is Admin:</strong>
                                {!! Form::checkbox('admin', 0, null, ['id' => 'admin']) !!}
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success add">
                        <span id="" class='glyphicon glyphicon-check'></span> Add
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End modal form to add a post -->

@endsection