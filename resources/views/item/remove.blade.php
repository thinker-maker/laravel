<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Are you sure?</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" role="form">
                <div class="form-group col-sm-12">                    
                    <p style="<?= $notItem ? 'display:none' : '' ?>">Do you really want to remove this item? This action cannot be reverted.</p>
                    <p style="<?= $notItem ? '' : 'display:none'?>">Deleting this record will affect {{$affected}} item records. <span style="<?= $affected < 1 ? 'display:none' : '' ?>"><?= $affected > 1 ? 'These items' : 'This item' ?> will be also deleted.</span></p>
                </div>                    
            </form>
            <div class="remove-modal-footer">
                <button type="button" class="btn btn-success remove" data-id="{{$id}}">
                    <span id="" class='glyphicon glyphicon-trash'></span>Remove
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <span class='glyphicon glyphicon-remove'></span>Close
                </button>
            </div>
        </div>
    </div>
</div>