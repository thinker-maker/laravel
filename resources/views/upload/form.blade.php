@if (count($errors) > 0)
    <ul>
        @foreach ($errors->all() as $error)
	    <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

<form action="/upload" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    Product name:
    <br />
    <input type="text" name="name" />
    <br /><br />
    Product photo:
    <br />
    <input type="file" name="photos" />
    <br /><br />
    <input type="submit" value="Upload" />
</form>
