<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Vendor model
 *
 * @author Pavlo Gumeniuk <pavlo.g@scopicsoftware.com>
 */
class Vendor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
        'name',
        'logo',        
    ];
    
    public $timestamps = false;

    /**
     * Get the user that owns the vendor.
     */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    /**
     * Get the items that the vendor owns.
     */
    public function items()
    {
    	return $this->hasMany(Items::class);
    }
}
