<!DOCTYPE html>
<html>
    <head>
        <title>Users</title>
        <!-- Favicon -->
	    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

	    <!-- CSFR token for ajax call -->
	    <meta name="_token" content="{{ csrf_token() }}"/>

	    <title>Manage Posts</title>

	    <!-- Bootstrap CSS -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

	    <!-- icheck checkboxes -->
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.css">

	    <!-- toastr notifications -->
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

	    <!-- Font Awesome -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <body>

        <div class="container">
	        @yield('content')
        </div>

    </body>
</html>