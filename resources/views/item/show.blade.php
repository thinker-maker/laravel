@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb pull-left">
            <h2>{{ $item->name}}</h2>
        </div>
    </div>
    <div class="row">
        @foreach($item->tags as $tag)
            <?php if(!isset($tag->name)) continue?>
            <label class="btn label label-info" data-name="$tag->name" onclick="window.location.href='{{ url('/item/getByTag/' . $tag->name ) }}'">{{ $tag->name }}</label>
        @endforeach
    </div>
    <div class="row show-item">
        <div class="col-xs-2 col-sm-2 col-md-2">
            <div class="row">
                <strong>Type:</strong>
            </div>
            <div class="row">
                <strong>Vendor:</strong>
            </div>
            <div class="row">
                <strong>Serial:</strong>
            </div>
            <div class="row">
                <strong>Weight:</strong>
            </div>
            <div class="row">
                <strong>Color:</strong>
            </div>
            <div class="row">
                <strong>Release Date:</strong>
            </div>
            <div class="row">
                <strong>Price:</strong>
            </div>
            <div class="row">
                <button onclick="window.location.href='{{ url('/item/edit/' . $item->id ) }}'" type="button" class="btn btn-primary">Edit</button>
                <button onclick="window.location.href='{{ url('/item/destroy/' . $item->id ) }}'" type="button" class="btn btn-danger">Delete</button>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="row">
                {{ $item->type->name}}
            </div>
            <div class="row">
                {{ $item->vendor->name}}
            </div>
            <div class="row">
                {{ $item->serial}}
            </div>
            <div class="row">
                {{ $item->weight}}
            </div>
            <div class="row"">
                <div class="color-box" style="background:#{{$item->color}}">
                </div>
            </div>
            <div class="row">
                {{ $item->release_date}}
            </div>
            <div class="row">
                {{ $item->price}}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group item">
                <img src="{{ URL::to('/images/items/' .$item->photo) }}">
            </div>
        </div>
    </div>
@endsection