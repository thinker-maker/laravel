@extends('layouts.app')

@section('content')
	<div class="row">
        <div class="col-lg-12 margin-tb pull-left">
            <h2>Items with tag {{ $tag}}</h2>
        </div>
    </div>
	<div class="row">
		@foreach ($items as $item)
            <li>
                <a href="{{ route('item.show', $item->id)}}">{{ $item->name }}</a>
                <div class="last-items-image">
                    <img src="{{ URL::to('/images/items/' .$item->photo) }}"" />
                </div>
            </li>
        @endforeach
	</div>
@endsection