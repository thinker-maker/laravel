<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * User model
 *
 * @author Pavlo Gumeniuk <pavlo.g@scopicsoftware.com>
 */
class User extends Authenticatable
{
    use Notifiable;
    
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Check if the user has admin access
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->admin ? true : false;
    }

    /**
     * Get the items that the user owns.
     */
    public function items()
    {
        return $this->hasMany(Items::class);
    }

    /**
     * Get the vendors that the user owns.
     */
    public function vendors()
    {
        return $this->hasMany(Vendor::class);
    }
}
