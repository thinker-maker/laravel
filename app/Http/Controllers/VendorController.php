<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UploadRequest;
use Illuminate\Support\Facades\DB;
use View;
use Yajra\Datatables\Datatables;
use App\Vendor;

/**
 * VendorController
 *
 * Controller for vendors pages.
 *
 * @author Pavlo Gumeniuk <pavlo.g@scopicsoftware.com>
 */
class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vendor.index');
    }

    /**
     * Render table with vendors
     * @return Yajra\Datatables\Datatables
     */
    public function tableData()
    {
        $vendor = DB::table('vendors');
        if (!\Auth::user()->admin) {
            $vendor->where('owner_id', '=', \Auth::user()->id);
        }
        
        return Datatables::of($vendor->get())
            ->editColumn('logo', '<div class="last-items"><img src="{{ URL::to(\'/images/vendors/\' .$logo) }}" /></div>')
            ->addColumn('action', '<a href="{{ route(\'vendor.edit\', $id)}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>  <a href="#" class="btn btn-xs btn-primary removeBtn" data-id="{{$id}}"><i class="glyphicon glyphicon-trash"></i> Delete</a>'
            )
            ->rawColumns(['logo', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.create', ['success' => false]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return view('vendor.create', ['errors' => $validator->messages()]);
        }

        if (request()->image) {
            $image_name = time().'.'.request()->image->getClientOriginalExtension();
            $request->image->move(public_path('images/vendors'), $image_name);
        }
        $imageName = isset($image_name) ? $image_name : 'NoImage.png';
        
        $vendor = new Vendor();
        $vendor->name = $request->name;
        $vendor->logo = $imageName;
        $vendor->owner_id = \Auth::user()->id;
        
        if ($vendor->save()) {
            return redirect()->route('vendor.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor = Vendor::findOrFail($id);

        if (!\Auth::user()->admin && $vendor->owner_id != \Auth::user()->id) {
            return redirect()->route('home');
        }

        return view('vendor.edit', ['vendor' => $vendor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vendor = Vendor::findOrFail($id);
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return view('vendor.edit', ['errors' => $validator->messages(), 'vendor' => $vendor]);
        }
        
        $imageName = $vendor->logo ? $vendor->logo : 'NoImage.png';
        if (request()->image) {
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            $request->image->move(public_path('images/vendors'), $imageName);
        }
        $vendor->name = $request->name;
        $vendor->logo = $imageName;
        $vendor->save();

        return redirect()->route('vendor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::findOrFail($id);

        if (\Auth::user()->admin || $vendor->owner_id == \Auth::user()->id) {
            
            foreach ($vendor->items as $item) {
                $item->delete();
            }

            $vendor->delete();
        }

        return redirect()->route('home');
    }

    /**
     * Renders pop-up with remove request
     * @param integer $id
     */
    public function confirmRemove($id)
    {
        $vendor = Vendor::findOrFail($id);
        $affected = 0;
        $notItem = true;
        $class = 'vendor';

        if ($vendor) {
            $affected = count($vendor->items);
        }

        $view = View::make('item/remove', compact('affected', 'notItem', 'id', 'class'));
        return $view->render();

    }
}
