@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <strong>Total Items Number:</strong>
                {{$data['total']}}
            </div>
            <br>
            <div class="row">
                <strong>Average Item Price:</strong>
                {{$data['avg']}}
            </div>
            <br>
            <div class="row">
            <strong>Las Added Items:</strong>
            <br>
            <?php foreach ($data['last'] as $item):?>
                <div class="row">
                    <div class="col-sm-4 last-items-image">
                        <img src="{{ URL::to('/images/items/' .$item->photo) }}">
                    </div>
                    <div class="col-sm-6 last-items">
                        <a href="{{ url('/item/show/' . $item->id ) }}"><strong>{{$item->name}}</strong></a>
                        <br>
                        Vendor: {{$item->vendor->name}}
                        <br>
                        <img src="{{ URL::to('/images/vendors/' .$item->vendor->logo) }}">
                        <br>
                        Price: {{$item->price}}
                        <br>
                        Tags: 
                        <?php 
                            // $tags = '';
                            // foreach (json_decode($item->tags) as $tag) {
                            //     $tags .= $tag . ', ';
                            // }
                            // echo $tags;
                        ?>
                    </div>
                </div>
            <?php endforeach?>
            </div>
        </div>
        <div class="col-sm-6">
            <div id="piechart_3d" style="width: 900px; height: 500px;"></div>
        </div>
    </div>

@endsection