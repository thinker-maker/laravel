<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Items;
use App\Vendor;
use App\Types;
use View;

/**
 * ItemController
 *
 * Controller for items' pages.
 *
 * @author Pavlo Gumeniuk <pavlo.g@scopicsoftware.com>
 */
class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        return view('item.index');
    }

    /**
     * Render table with items
     * @return Yajra\Datatables\Datatables
     */
    public function tableData()
    {
        $items = Items::with('vendor', 'type');
        if (!\Auth::user()->admin) {
            $items->where('owner_id', '=', \Auth::user()->id);
        }
        
        return Datatables::of($items->get())
            ->editColumn('name', '<a href="{{ route(\'item.show\', $id)}}">{{$name}}</a>')
            ->editColumn('color', '<div>{{$color}}<span class="color-box" style="background:#{{$color}}"></span></div>')
            ->editColumn('photo', '<div class="last-items"><img src="{{ URL::to(\'/images/items/\' .$photo) }}" /></div>')
            ->addColumn('action', '<a href="{{ route(\'item.edit\', $id)}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>  <a href="#" class="btn btn-xs btn-primary removeBtn" data-id="{{$id}}"><i class="glyphicon glyphicon-trash"></i> Delete</a>')
            ->rawColumns(['name', 'color', 'photo', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendor = Vendor::where('owner_id', \Auth::user()->id)->pluck('name', 'id');
        $type = Types::pluck('name', 'id');
        $ex_tags = Items::existingTags();
        $tags = [];
        foreach ($ex_tags as $ex_tag) {
            $tags[] = $ex_tag["name"];
        }
        $tags = json_encode($tags);

        return view('item.create', compact('vendor', 'type', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price' => 'required|numeric|regex:/^\d*(\.\d{2})?$/',
            'weight' => 'numeric|regex:/^\d*(\.\d{2})?$/',
            'vendor_id' => 'required',
            'type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return \Redirect::back()->with(['errors' => $validator->messages()])->withInput($request->input());
        }

        if (request()->image) {
            $image_name = time().'.'.request()->image->getClientOriginalExtension();
            $request->image->move(public_path('images/items'), $image_name);
        }
        $imageName = isset($image_name) ? $image_name : 'NoImage.png';
        
        $item = new Items();
        $item->name = $request->name;
        $item->photo = $imageName;
        $item->owner_id = \Auth::user()->id;
        $item->vendor_id = $request->vendor_id;
        $item->type_id = $request->type_id;
        $item->serial = $request->serial;
        $item->price = $request->price;
        $item->weight = $request->weight;
        $item->color = $request->color;        
        $item->release_date = $request->release_date;
        
        if ($item->save()) {
            if($request->hiddentags){
                $item->tag(explode(",", $request->hiddentags));
                $item->save();
            }
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Items::findOrFail($id);
        if (!\Auth::user()->admin && $item->owner_id != \Auth::user()->id) {
            return redirect()->route('home');
        }

        return view('item.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Items::with('tagged')->find($id);
        $item->tags = $item->tagNames();
        if (!\Auth::user()->admin && $item->owner_id != \Auth::user()->id) {
            return redirect()->route('home');
        }

        $vendor = Vendor::pluck('name', 'id');
        $type = Types::pluck('name', 'id');
        $ex_tags = Items::existingTags();
        $tags = [];
        foreach ($ex_tags as $ex_tag) {
            $tags[] = $ex_tag["name"];
        }
        $tags = json_encode($tags);

        return view('item.edit', compact('item', 'vendor', 'type', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Items::findOrFail($id);
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price' => 'required|numeric|regex:/^\d*(\.\d{2})?$/',
            'weight' => 'numeric|regex:/^\d*(\.\d{2})?$/',
        ]);

        $ex_tags = Items::existingTags();
        $tags = [];
        foreach ($ex_tags as $ex_tag) {
            $tags[] = $ex_tag["name"];
        }
        $tags = json_encode($tags);

        if ($validator->fails()) {
            $vendor = Vendor::pluck('name', 'id');
            $type = Types::pluck('name', 'id');
            return view('item.edit', [
                'errors' => $validator->messages(), 
                'item' => $item,
                'vendor' => $vendor,
                'type' => $type,  
                'tags' => $tags,              
            ]);
        }
        
        $imageName = $item->photo ? $item->photo : 'NoImage.png';
        if (request()->image) {
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            $request->image->move(public_path('images/items'), $imageName);
        }
        $item->name = $request->name;
        $item->photo = $imageName;
        $item->owner_id = \Auth::user()->id;
        $item->vendor_id = $request->vendor_id;
        $item->type_id = $request->type_id;
        $item->serial = $request->serial;
        $item->price = $request->price;
        $item->weight = $request->weight;
        $item->color = $request->color;
        $tags = explode(",", $request->hiddentags);
        if ($tags) {
            foreach($tags as $tag){
                $item->tag([$tag]);
            }
        }
        
        $item->release_date = $request->release_date;
        $item->save();

        return redirect()->route('item.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Items::findOrFail($id);

        if (\Auth::user()->admin || $item->owner_id == \Auth::user()->id) {
            $item->delete();
        }

        return redirect()->route('home');
    }

    /**
     * Removing tag
     * @param integer $id 
     * @param string $tag 
     * @return boolean
     */
    public function removeTag($id, $tag)
    {
        $item = Items::with('tagged')->where('id', $id)->get();
        return $item[0]->untag([$tag]);
    }

    /**
     * Renders page with items after a certain tag
     * @param string $tag
     */
    public function getByTag($tag)
    {
        return view('item.tagged', ["items" => Items::withAnyTag([$tag])->get(), "tag" => $tag]);
    }

    /**
     * Renders pop-up with remove request
     * @param integer $id
     */
    public function confirmRemove($id)
    {
        $item = Items::findOrFail($id);
        $affected = 0;
        $notItem = false;
        $class = 'item';

        $view = View::make('item/remove', compact('affected', 'notItem', 'id', 'class'));
        return $view->render();

    }
}
