@extends('layouts.app')

@section('content')
<div class="row">
    <p class="errorTitle text-left alert alert-danger hidden"></p>
    <p class="successTitle text-left alert alert-success hidden"></p>
    <table class="table table-bordered" id="types-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<div class="row" <?= \Auth::user()->admin ? '' : 'style="display:none"'?>>
    <a href="{{ route('type.create')}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i> Create Type</a>
</div>
<div class="modal fade removeModal" role="dialog">
</div>
<script>
    var place = 'type';
</script>
@endsection