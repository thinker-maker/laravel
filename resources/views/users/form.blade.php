<div class="row">
    <div class="col-sm-7">
        <div class="form-group">
            <strong>Username:</strong>
            {!! Form::text('username', null, array('placeholder' => 'Username','class' => 'form-control')) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-7">
        <div class="form-group">
            <strong>Email:</strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-7">
        <div class="form-group">
            <strong>Password:</strong>
            {!! Form::password('password', null, array('placeholder' => 'Password','class' => 'form-control')) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-7">
        <div class="form-group">
            <strong>Repeat Password:</strong>
            {!! Form::password('password_confirmation', null, array('placeholder' => 'Repeat Password','class' => 'form-control')) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-7">
        <div class="form-group">
            <strong>Active:</strong>
            {!! Form::checkbox('active', 1, null) !!}
        </div>
    </div>
    <div class="col-sm-7">
        <div class="form-group">
            <strong>Is Admin:</strong>
            {!! Form::checkbox('admin', null) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 text-left">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>