<!-- @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif -->
<div class="form-group">
    {!! Form::label('Item Name') !!}
    {!! Form::text('name', null, array('placeholder'=>'Name')) !!}
</div>

<div class="form-group">
    {!! Form::label('Vendor') !!}
    {!! Form::select('vendor_id', $vendor, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('Type') !!}
    {!! Form::select('type_id', $type, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('Serial') !!}
    {!! Form::text('serial', null, array('placeholder'=>'Name')) !!}
</div>

<div class="form-group">
    {!! Form::label('Price') !!}
    {!! Form::text('price', null, array('placeholder'=>'Name')) !!}
</div>

<div class="form-group">
    {!! Form::label('Weight') !!}
    {!! Form::text('weight', null, array('placeholder'=>'Name')) !!}
</div>

<div class="form-group">
    {!! Form::label('Color') !!}
    {!! Form::text('color', null, array('class'=>'jscolor')) !!}
</div>

<div class="form-group">
    {!! Form::label('Release Date') !!}
    {!! Form::text('release_date', '', array('id' => 'datepicker')) !!}
</div>

<div class="form-group">
    <label>Tags:</label>
    @if(isset($item))
        @foreach($item->tags as $tag)
            <?php if(!isset($tag->name)) continue;?>
            <label class="label label-info">{{ $tag->name }} <i class="tag-remove" data-id="{{$item->id}}" data-name="{{$tag->name}}">x</i></label>
        @endforeach
    @endif
    <br/>
    <div class="form-group">
        <label>Add Tags:</label><br/>
        <input type="text" name="tags" placeholder="Tags" class="typeahead tm-input form-control tm-input-info"/>
    </div>

<div class="form-group">
    <input type="file" name="image" id="image" >
</div>

<div class="form-group">
    {!! Form::submit('Submit') !!}
</div>
<script type="text/javascript">
    console.log('{!! $tags !!}');
    var tags = {!! $tags !!};
    var baseDate = '{!! isset($item->release_date) ? $item->release_date : date('Y-m-d')!!}';
</script>