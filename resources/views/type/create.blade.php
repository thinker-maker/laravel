@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Type</h2>
            </div>
        </div>
    </div>

    @if ($success)
        <div class="alert alert-success">
            Type created.<br><br>
        </div>
    @endif

    @if (count($errors) < 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(array('route' => 'type_store','method'=>'POST')) !!}
         @include('type.form')
    {!! Form::close() !!}

@endsection