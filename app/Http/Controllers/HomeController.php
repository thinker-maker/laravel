<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Items;
use App\Vendor;
use App\Types;

/**
 * HomeController
 *
 * Basic controller.
 *
 * @author Pavlo Gumeniuk <pavlo.g@scopicsoftware.com>
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!\Auth::user()) {
            return view('home');
        } else {
            return  \Auth::user()->admin ? 
                redirect()->route('dashboard') : 
                redirect()->route('item.index');
        }
    }

    /**
     * Renders dashboard with statistics for 
     * users with admin access
     */
    public function dashboard()
    {
        $types = Types::all();
        $data = [];
        $count[] = ["Type", "Items per Type"];
        if (!empty($types)) {
            foreach ($types as $type) {
                $count[] = [$type->name, Items::where('type_id', '=', $type->id)->count()];
            }
        }
        $data['total'] = Items::count();
        $data['sum'] = Items::sum('price');
        $data['avg'] = $data['total'] ? ($data['sum'] / $data['total']) : 0;
        $data['last'] = Items::orderBy('created_at', 'DESC')->take(5)->get();

        return view('dashboard', compact('count', 'data'));
    }
}