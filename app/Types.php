<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Types model
 *
 * @author Pavlo Gumeniuk <pavlo.g@scopicsoftware.com>
 */
class Types extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    /**
     * Get the items that the type owns.
     */
    public function items()
    {
    	return $this->hasMany(Items::class);
    }
}
