<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');



Route::group(['middleware' => 'can:accessAdminpanel'], function() {

	    Route::get('/users', ['as' => 'users', 'uses' => 'UsersController@index']);
	    Route::post('/users/changeStatus', 'UsersController@changeStatus');
	    Route::post('/users/changeState', 'UsersController@changeState');
	    Route::post('/users/store', 'UsersController@store');

	    Route::get('/type', 'TypesController@index')->name('type.index');
	    Route::post('/type/tableData', 'TypesController@tableData')->name('type.data');
	    Route::get('/type/create', 'TypesController@create')->name('type.create');
	    Route::post('/type/store', ['as' => 'type_store', 'uses' => 'TypesController@store']);
	    Route::get('/type/edit/{id}', 'TypesController@edit')->name('type.edit');
	    Route::patch('/type/update/{id}', ['as' => 'type_update', 'uses' => 'TypesController@update']);
	    Route::get('/type/destroy/{id}', 'TypesController@destroy');
	    Route::get('/type/confirmRemove/{id}', 'TypesController@confirmRemove')->name('type.confirm');

	    Route::get('/home/dashboard', 'HomeController@dashboard')->name('dashboard');
	}
);

Route::group(['middleware' => 'can:accessProfile'], function() {

        Route::get('/vendor/create', 'VendorController@create')->name('vendor.create');
	    Route::post('/vendor/store', ['as' => 'vendor.store', 'uses' => 'VendorController@store']);	    
	    
	    Route::get('/item/create', 'ItemController@create')->name('item.create');
	    Route::post('/item/store', ['as' => 'item.store', 'uses' => 'ItemController@store']);	    
	    
    }
);

Route::group(['middleware' => 'can:accessRegistered'], function() {

        Route::get('/vendor', 'VendorController@index')->name('vendor.index');
        Route::get('/vendor/edit/{id}', 'VendorController@edit')->name('vendor.edit');
        Route::post('/vendor/tableData', 'VendorController@tableData');
        Route::post('/vendor/update/{id}', ['as' => 'vendor.update', 'uses' => 'VendorController@update']);
        Route::get('/vendor/confirmRemove/{id}', 'VendorController@confirmRemove')->name('vendor.confirm');
        Route::get('/vendor/destroy/{id}', 'VendorController@destroy')->name('vendor.remove');

        Route::get('/item', 'ItemController@index')->name('item.index');
		Route::post('/item/tableData', 'ItemController@tableData')->name('tables.data');

        Route::get('/item/edit/{id}', 'ItemController@edit')->name('item.edit');
        Route::post('/item/update/{id}', ['as' => 'item.update', 'uses' => 'ItemController@update']);
        Route::get('/item/edit/removeTag/{id}/{tag}', 'ItemController@removeTag');

        Route::get('/item/show/{id}', 'ItemController@show')->name('item.show');
        Route::get('/item/getByTag/{tag}', 'ItemController@getByTag');

        Route::get('/item/destroy/{id}', 'ItemController@destroy');
        Route::get('/item/confirmRemove/{id}', 'ItemController@confirmRemove')->name('item.confirm');

   }
);