@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Item</h2>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    {!! Form::model($item, ['method' => 'POST','route' => ['item.update', $item->id], 'enctype' => 'multipart/form-data']) !!}
        <img src="{{ URL::to('/images/items/' .$item->photo) }}">
        @include('item.form')        
    {!! Form::close() !!}

@endsection