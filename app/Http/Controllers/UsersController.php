<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use Response;

/**
 * UsersController
 *
 * Controller to manage users.
 *
 * @author Pavlo Gumeniuk <pavlo.g@scopicsoftware.com>
 */
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     
        $users = User::all();
        
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'username' => 'required|string|max:255',
            'email' => 'string|email|max:255',
            'password' => 'required|string|min:4|confirmed',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }
        $user = new User();
        $user::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'admin' => $request->admin,
            'active' => $request->active,
        ]);
        return response()->json(User::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'username' => 'required|string|max:255',
            'email' => 'string|email|max:255|unique:users,id,'.$id,
            'password' => 'required|string|min:4|confirmed',
        ]);
        User::find($id)->update([
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'admin' => $request->admin,
            'active' => $request->active,
        ]);
        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->active = 0;
        $user->save();
        return redirect()->route('users.index')
                        ->with('success','User deactivated successfully');
    }

    /**
     * Change user.active value
     * 
     * @param Request $request 
     * @return json
     */
    public function changeStatus(Request $request) 
    {
        $user = User::findOrFail($request->id);
        $user->active = !$user->active;
        if ($user->save()) {
            return response()->json($user->active);
        } else {
            return response()->json('Not saved', 200);
        }
    }

    /**
     * Change user.admin value
     * 
     * @param Request $request 
     * @return json
     */
    public function changeState(Request $request) 
    {
        $user = User::findOrFail($request->id);
        $user->admin = !$user->admin;
        if ($user->save()) {
            return response()->json($user->admin);
        } else {
            return response()->json('Not saved', 200);
        }
    }
}
