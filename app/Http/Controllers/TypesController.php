<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Items;
use App\Types;

/**
 * TypesController
 *
 * Controller for types pages.
 *
 * @author Pavlo Gumeniuk <pavlo.g@scopicsoftware.com>
 */
class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('type.index');
    }

    /**
     * Render table with types
     * @return Yajra\Datatables\Datatables
     */
    public function tableData()
    {
        $types = DB::table('types');
                
        return Datatables::of($types->get())
            ->addColumn('action', '<a href="{{ route(\'type.edit\', $id)}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>  <a href="#" class="btn btn-xs btn-primary removeBtn" data-id="{{$id}}"><i class="glyphicon glyphicon-trash"></i> Delete</a>')
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('type.create', ['success' => false]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return view('type.create', ['success' => false]);
        }
        $type = new Types();
        $type->name = $request->name;
        if ($type->save()) {
            return redirect()->route('type.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = Types::findOrFail($id);
        return view('type.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required|string|max:255',
        ]);
        Types::find($id)->update([
            'name' => $request->name,
        ]);
        
        return redirect()->route('type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = Types::findOrFail($id);

        if (\Auth::user()->admin || $type->owner_id == \Auth::user()->id) {
            $items = Items::where('type_id', $id)->get();
            foreach ($items as $item) {
                $item->delete();
            }

            $type->delete();
        }

        return redirect()->route('home');
    }

    /**
     * Renders pop-up with remove request
     * @param integer $id
     */
    public function confirmRemove($id)
    {
        $type = Types::findOrFail($id);
        $affected = 0;
        $notItem = true;
        $class = 'type';

        if ($type) {            
            $affected = count(Items::where('type_id', $id)->get());
        }

        $view = View::make('item/remove', compact('affected', 'notItem', 'id', 'class'));

        return $view->render();
    }
}
